$('.jsTopSlider').on('init', function(slick) {
    $(this).find($('.slick-dots')).wrap('<div class="c-top__slider-dots-wrap"><div class="c-top__slider-dots-table"></div></div>');
});
$('.jsTopSlider').slick({
  slidesToShow: 1,
  dots: true,
  infinite: true,
  speed: 300,
  fade: true,
  cssEase: 'linear',
  arrows: 0
});

setTime();

setTimeout(function run() {
  setTime()
  setTimeout(run, 1000);
}, 1000);


function setTime() {
    var curTime = currentTime();
    $('#lalatime').text(curTime);
}
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
function currentTime() {
    var d = new Date();
    var x = document.getElementById("demo");
    var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    return  h + ":" + m + ":" + s;
}
